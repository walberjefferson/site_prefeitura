$( document ).ready(function(){

    $('.slider').slider({
        interval: 3000,
        transition: 1000,
        height: 400
    });

    $('ul.tabs').tabs();

    $('.carousel').hide();
    $('.carousel.carousel-slider').carousel({
        fullWidth: false,
        indicators: true,
        duration: 50,
        shift: 100
    });

    var topoNoticia = $('#noticias').offset().top;
    var topoFique = $('#fique').offset().top;
    // console.log(topoFique);
    $('#noticias, #acesso, #fique').hide();

    $(window).scroll(function () {
        //300
        var scrollTop = $(this).scrollTop();

        if(scrollTop >= topoNoticia - 535){
            $('#noticias, #acesso').slideDown(300);
        }else {
            $('#noticias, #acesso').slideUp(300);
        }

        if(scrollTop >= topoFique - 490){
            $('#fique').slideDown(300);
        }else {
            $('#fique').slideUp(300);
        }
    });



});
